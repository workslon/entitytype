# Overview

Nodejs module - adapted version of [`eNTITYtYPE.js` library](https://bitbucket.org/workslon/ontojs/src/8367a54d7dbdc4330a750dfdfbbd9a3e27e17867/src/eNTITYtYPE.js) written by Prof. Gerd Wagner, BTU Cottbus

You can find the source code [here](https://bitbucket.org/workslon/entitytype/src/69d268a14af87d5fd08e3d76769747f578805694/index.js)

# Install

## ssh

```
npm install git+ssh://git@bitbucket.org/workslon/entitytype.git --save
```

## https

```
npm install https://git@bitbucket.org/workslon/entitytype.git --save
```

# Usage

## CommonJS

```javascript
var eNTITYtYPE = require('eNTITYtYPE');

// ...
```

## ES6 Modules

```javascript
import eNTITYtYPE from 'eNTITYtYPE';

// ...
```