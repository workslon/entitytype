/**
 * @fileOverview  This file contains the definition of the abstract
 * meta-class eNTITYtYPE.
 * @author Gerd Wagner
 * @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
 /**
 * Abstract meta-class for deriving the meta-classes mODELcLASS, oBJECTtYPE, etc.
 * with a create method for entity creation. eNTITYtYPE defines the following
 * type features: typeName, supertype(s), properties, methods (including the
 * predefined methods toDisplayString, toRecord, set and isInstanceOf),
 * isNonPersistent,
 * TODO: storageAdapter
 * TODO: staticMethods,
 * and the agent type features reactionRules and internalEventTypes.
 *
 * By default, properties are mandatory, if there is no declaration slot
 * "optional:true". The property declaration parameter "minCard" is only
 * meaningful for multi-valued properties, for which "maxCard" has been
 * set to a value greater than 1.
 *
 * On entity type creation, the constructor argument "methods" may include
 * a custom object-level "validate" method returning an error message string
 * in case of an object-level constraint violation.
 *
 * TODO: New features for managing events in the context of observer entities:
 * (preferably return context entity, so calls can be chained)
 * + addEventListener( eventTypeName,[ targetEntity,] listenerMethod)
 *       If no targetEntity provided: targetEntity = observerEntity
 * + removeEventListener( eventTypeName,[ targetEntity,] listenerMethod)
 * + eventListeners( eventTypeName)
 *       Returns an array of listeners for the specified event type.
 * + emitEvent( eventTypeName[, arg1][, arg2][, ...])
 *       Execute all listeners in order with the supplied arguments.
 * + Built-in events:
 *   + Entity lifecycle events (Creation, Change, Destruction)
 *   + Collection events (Addition, Removal)
 *   + I/O events ...?
 *
 * @constructor
 * @this {eNTITYtYPE}
 * @param {{typeName: string, supertype: string?, supertypes: string?,
 *          properties: Map, methods: Map?, staticMethods: Map?,
 *          reactionRules: Map?, internalEventTypes: Map?}}
 *        slots  The entity type definition slots.
 */

var util = require('gw-util');
var errorTypes = require('error-types');
var cOMPLEXtYPE = require('cOMPLEXtYPE');
var createIsoDateString = util.createIsoDateString;
var eNTITYtYPEconstraintViolation = errorTypes.eNTITYtYPEconstraintViolation;
var RangeConstraintViolation = errorTypes.RangeConstraintViolation;
var NoConstraintViolation = errorTypes.NoConstraintViolation;
var UniquenessConstraintViolation = errorTypes.UniquenessConstraintViolation;
var MandatoryValueConstraintViolation = errorTypes.MandatoryValueConstraintViolation;

function eNTITYtYPE( slots) {
  var k=0, keys=[], supertype=null,
      featureMaps = {properties:{}, methods:{}, staticMethods:{},
          reactionRules:{}, internalEventTypes:{}};
  // check entity property range and set standardId
  Object.keys( slots.properties).forEach( function (prop) {
    var propDeclParams = slots.properties[prop];  // the property declaration slots
    // check if properties are declared with a range
    if (!propDeclParams.range && !(slots.methods && "validate" in slots.methods))
      throw new eNTITYtYPEconstraintViolation(
          "Either there must be a range definition for "+ prop +
          " or its's range must be checked in a 'validate' method!");
    if (propDeclParams.isStandardId || prop === "id") this.standardId = prop;
  }, this);

  // invoke the cOMPLEXtYPE constructor since an eNTITYtYPE is a cOMPLEXtYPE
  cOMPLEXtYPE.call( this, slots);

  // TODO: Is this really needed? Or are non-persistent cases complex datatypes?
  if (slots.isNonPersistent) {
    this.isNonPersistent = slots.isNonPersistent;
  }
  // add predefined methods such that they will be inherited by all subtypes
  if (!slots.supertype && !slots.supertypes) {  // root class (no inheritance)
    // **************************************************
    this.methods.convertPropValToString = function (p) {
    // **************************************************
      var mODELcLASS = mODELcLASS || undefined;
      var valuesToConvert=[], displayStr="", k=0, listSep = ", ",
        obj = this, val = obj[p],
          propDecl = obj.type.properties[p],
      range = propDecl.range;
      if (val === undefined) return "";
      if (propDecl.maxCard && propDecl.maxCard > 1) {
        if (mODELcLASS && range instanceof mODELcLASS) { // object reference(s)
          if (Array.isArray( val)) {
            valuesToConvert = val.slice(0);  // clone;
          } else {  // map from ID refs to obj refs
            valuesToConvert = Object.values( val);
          }
        } else if (Array.isArray( val)) {
          valuesToConvert = val.slice(0);  // clone;
        } else console.log("Invalid non-array collection in to Record!");
      } else {
        valuesToConvert = [val];
      }
      valuesToConvert.forEach( function (v,i) {
        if (range instanceof eNUMERATION) {
          valuesToConvert[i] = range.labels[v-1];
        } else if (["number","string","boolean"].includes( typeof(v)) || !v) {
          valuesToConvert[i] = String( v);
        } else if (range === "Date") {
          valuesToConvert[i] = util.createIsoDateString( v);
        } else if (Array.isArray( v)) {  // JSON-compatible array
          valuesToConvert[i] = v.slice(0);  // clone
        } else if (range instanceof cOMPLEXdATAtYPE) {
          valuesToConvert[i] = v.toDisplayString();
        } else if (mODELcLASS && range instanceof mODELcLASS) { // object reference(s)
          valuesToConvert[i] = v[range.standardId];
        } else valuesToConvert[i] = JSON.stringify( v);
      });
      displayStr = valuesToConvert[0];
      if (propDecl.maxCard && propDecl.maxCard > 1) {
        for (k=1; k < valuesToConvert.length; k++) {
          displayStr += listSep + valuesToConvert[k];
        }
      }
      return displayStr;
    };
    // **************************************************
    this.methods.toRecord = function () {
    // **************************************************
      var mODELcLASS = mODELcLASS || undefined;
      var obj = this, rec={}, propDecl={}, valuesToConvert=[], range, val;
      Object.keys( obj).forEach( function (p) {
        if (p !== "type" && obj[p] !== undefined) {
          val = obj[p];
          propDecl = obj.type.properties[p];
          range = propDecl.range;
          if (propDecl.maxCard && propDecl.maxCard > 1) {
            if (mODELcLASS && range instanceof mODELcLASS) { // object reference(s)
              if (Array.isArray( val)) {
                valuesToConvert = val.slice(0);  // clone;
              } else {  // map from ID refs to obj refs
                valuesToConvert = Object.values( val);
              }
            } else if (Array.isArray( val)) {
              valuesToConvert = val.slice(0);  // clone;
            } else console.log("Invalid non-array collection in to Record!");
          } else {  // maxCard=1
            valuesToConvert = [val];
          }
          valuesToConvert.forEach( function (v,i) {
            // alternatively: enum literals as labels
            // if (range instanceof eNUMERATION) rec[p] = range.labels[val-1];
            if (["number","string","boolean"].includes( typeof(v)) || !v) {
              valuesToConvert[i] = String( v);
            } else if (range === "Date") {
              valuesToConvert[i] = util.createIsoDateString( v);
            } else if (range instanceof cOMPLEXdATAtYPE) {
              valuesToConvert[i] = v.toRecord();
            } else if (mODELcLASS && range instanceof mODELcLASS) { // object reference(s)
              valuesToConvert[i] = v[range.standardId];
            } else if (Array.isArray( v)) {  // JSON-compatible array
              valuesToConvert[i] = v.slice(0);  // clone
            } else valuesToConvert[i] = JSON.stringify( v);
          });
          if (!propDecl.maxCard || propDecl.maxCard <= 1) {
            rec[p] = valuesToConvert[0];
          } else {
            rec[p] = valuesToConvert;
          }
        }
      });
      return rec;
    };
  }
}
eNTITYtYPE.prototype = Object.create( cOMPLEXtYPE.prototype);
eNTITYtYPE.prototype.constructor = eNTITYtYPE;

/**
 * Convert property value to form field value.
 * @method
 * @author Gerd Wagner
 * @param {eNTITYtYPE} ET  The domain of the property.
 * @param {string} prop  The property name.
 * @param {?} v  The value to be converted.
 * @return {boolean}
 */
eNTITYtYPE.convertPropValToString = function ( ET, prop, v) {
  var mODELcLASS = mODELcLASS || undefined;
  var displayStr="",
      propDecl = ET.properties[prop], range = propDecl.range;
    if (v === undefined) return "";
    if (range instanceof eNUMERATION) return range.labels[v-1];
    if (["number","string","boolean"].includes( typeof(v))) {
        return String( v);
    }
    if (range === "Date") return util.createIsoDateString( v);
    if (mODELcLASS && range instanceof mODELcLASS) return v[range.standardId];
    return JSON.stringify( v);
};
/**
 * Generic method for checking the integrity constraints defined in property declarations.
 * The values to be checked are first parsed/converted if provided as strings.
 *
 * min/max: numeric (or string length) minimum/maximum
 * optional: true if property is single-valued and optional (false by default)
 * range: String|NonEmptyString|Integer|...
 * pattern: a regular expression to be matched
 * minCard/maxCard: minimum/maximum cardinality of a multi-valued property
 *     By default, maxCard is 1, implying that the property is single-valued, in which
 *     case minCard is meaningless/ignored. maxCard may be Infinity.
 * unique: property is unique
 * isStandardId: property is standard identifier
 *
 * @method
 * @author Gerd Wagner
 * @param {string} prop  The property for which a value is to be checked.
 * @param {string|number|boolean|object} val  The value to be checked.
 * @return {ConstraintViolation}  The constraint violation object.
 */
eNTITYtYPE.prototype.check = function (prop, val) {
  var mODELcLASS = mODELcLASS || undefined;
  var propDeclParams = this.properties[prop],
      constrVio=null, valuesToCheck=[], i=0, keys=[], range,
      maxCard = 1;  // by default, a property is single-valued

  // invoke the cOMPLEXtYPE check method since an eNTITYtYPE is a cOMPLEXtYPE
  constrVio = cOMPLEXtYPE.prototype.check.call( this, prop, val);
  if (!(constrVio instanceof NoConstraintViolation)) return constrVio;

  range = propDeclParams.range;
  maxCard = propDeclParams.maxCard || maxCard;
  if (maxCard === 1) {  // single-valued property
    valuesToCheck = [val];
  } else {  // multi-valued property
    // can be array-valued or map-valued
    if (Array.isArray(val) ) {
      valuesToCheck = val;
    } else if (typeof(val) === "object" && Object.keys( val).length > 0) {
      valuesToCheck = Object.keys( val); // TODO
    } else if (typeof(val) === "string") {
      valuesToCheck = val.split(",").map(function (el) {
        return el.trim();
      });
    } else {
      return new RangeConstraintViolation("Values for "+ prop +
      " must be arrays or maps!");
    }
  }
  if (mODELcLASS && range instanceof mODELcLASS) {
    valuesToCheck.forEach( function (v,i) {
      if (Number.isInteger(v)) v = String(v);
      if (typeof(v) === "string") {
        if (range.instances[v] !== undefined) {
          v = range.instances[v];
          valuesToCheck[i] = v;
        } else {
          constrVio = new RangeConstraintViolation( v +" is not a valid ID ref in "+
              range.name +".instances, as required for "+ prop +" !");
        }
      }
      if (typeof(v) !== "object" || v.type === undefined || v.type !== range) {
        constrVio = new RangeConstraintViolation("The value of "+ prop +
            " must reference an existing "+ range.name +"! "+
            JSON.stringify(v) +" does not!");
      }
    });
  }
  // return constraint violation if there is any
  if (constrVio) return constrVio;
  /********************************************************
   ***  Check uniqueness constraints  *********************
   ********************************************************/
  if (propDeclParams.unique && this.instances) {
    keys = Object.keys( this.instances);
    for (i=0; i < keys.length; i++) {
      if (this.instances[keys[i]][prop] === val) {
        return new UniquenessConstraintViolation("There is already a "+
            this.name +" with a(n) "+ prop +" value "+ val +"!");
      }
    }
  }
  if (propDeclParams.isStandardId) {
    if (val === undefined) {
      return new MandatoryValueConstraintViolation("A value for the " +
          "standard identifier attribute "+ prop +" is required!");
    }
    /*TODO
    else if (this.instances && this.instances[val]) {
      return new UniquenessConstraintViolation("There is already a "+
          this.name +" with a(n) "+ prop +" value "+ val +"!");
    }
    */
  }
  val = maxCard === 1 ? valuesToCheck[0] : valuesToCheck;
  return new NoConstraintViolation( val);
};
/**
 * ??? Really Needed ???
 * Generic method for checking uniqueness/stdid constraints defined in
 * property declarations with
 * unique: property is unique
 * isStandardId: property is standard identifier
 *
 * @method
 * @author Gerd Wagner
 * @param {string} prop  The property for which a value is to be checked.
 * @param {string|number|object} val  The value to be checked.
 * @return {ConstraintViolation}  The constraint violation object.
 */
eNTITYtYPE.prototype.checkUniqueness = function (prop, val) {
  var propDeclParams = this.properties[prop],
      i=0, keys=[];
  if (propDeclParams.unique && this.instances) {
    keys = Object.keys( this.instances);
    for (i=0; i < keys.length; i++) {
      if (this.instances[keys[i]][prop] === val) {
        return new UniquenessConstraintViolation("There is already a "+
            this.name +" with a(n) "+ prop +" value "+ val +"!");
      }
    }
  }
  if (propDeclParams.isStandardId) {
    if (val === undefined) {
      return new MandatoryValueConstraintViolation("A value for the " +
          "standard identifier attribute "+ label +" is required!");
    } else if (this.instances && this.instances[val]) {
      return new UniquenessConstraintViolation("There is already a "+
          this.name +" with a(n) "+ label +" value "+ val +"!");
    }
  }
  return new NoConstraintViolation();
};
/**
 * Generic method for converting rows/records to model objects
 * @method
 * @author Gerd Wagner
 * @param {object} record  The record/row to be converted
 */
eNTITYtYPE.prototype.convertRec2Obj = function (record) {
  var obj={};
  try {
    obj = this.create( record);
  } catch (e) {
    console.log( e.constructor.name + " while deserializing a "+
        this.name +" record: " + e.message);
    obj = null;
  }
  return obj;
};

module.exports = eNTITYtYPE;